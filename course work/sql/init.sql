\connect postgres

DROP DATABASE social;

CREATE DATABASE social;

\connect social

CREATE TABLE Users (
	id serial,
	email TEXT UNIQUE,
	password TEXT NOT NULL,
	firstname TEXT NOT NULL,
	secondname TEXT NOT NULL,
	city integer NOT NULL,
	about TEXT ,
	birthday timestamp ,
	datacreate timestamp ,
	vk_url TEXT ,
	twitter_url TEXT ,
	phone TEXT ,
	sex TEXT NOT NULL,
	type integer DEFAULT '1',
	popularity FLOAT  DEFAULT '0',
	activities FLOAT  DEFAULT '0',
	CONSTRAINT Users_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE UserType (
	id serial NOT NULL,
	type TEXT NOT NULL,
	CONSTRAINT UserType_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Friendinvites (
	id serial NOT NULL,
	from_u integer NOT NULL,
	to_u integer NOT NULL,
	CONSTRAINT Friendinvites_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Friends (
	id serial NOT NULL,
	user_id integer NOT NULL,
	friend_id integer NOT NULL,
	CONSTRAINT Friends_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Chat (
	id serial NOT NULL,
	name TEXT NOT NULL,
	created_at timestamp NOT NULL,
	created_by integer NOT NULL,
	level_chat integer NOT NULL,
	numinchat integer NOT NULL DEFAULT '0',
	CONSTRAINT Chat_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Invites (
	id serial NOT NULL,
	chat_id integer NOT NULL,
	created_at timestamp NOT NULL,
	from_u integer NOT NULL,
	to_u integer NOT NULL,
	CONSTRAINT Invites_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Message (
	id serial NOT NULL,
	autor_id integer NOT NULL,
	chat_id integer NOT NULL,
	created_at timestamp NOT NULL,
	text TEXT NOT NULL,
	CONSTRAINT Message_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE UserInChat (
	id serial NOT NULL,
	user_id integer NOT NULL,
	chat_id integer NOT NULL,
	created_by integer NOT NULL,
	created_at timestamp NOT NULL,
	user_level integer NOT NULL,
	CONSTRAINT UserInChat_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Conference (
	id serial NOT NULL,
	city integer NOT NULL,
	datetime timestamp NOT NULL,
	chat integer NOT NULL,
	CONSTRAINT Conference_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Report (
	id serial NOT NULL,
	url TEXT NOT NULL,
	autor integer NOT NULL,
	datetime timestamp NOT NULL,
	conference integer NOT NULL,
	CONSTRAINT Report_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE City (
	id serial NOT NULL,
	name TEXT NOT NULL UNIQUE,
	country integer NOT NULL,
	CONSTRAINT City_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Country (
	id serial NOT NULL,
	name TEXT NOT NULL UNIQUE,
	CONSTRAINT Country_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);




ALTER TABLE Users ADD CONSTRAINT Users_fk0 FOREIGN KEY (city) REFERENCES City(id);
ALTER TABLE Users ADD CONSTRAINT Users_fk1 FOREIGN KEY (type) REFERENCES UserType(id);

ALTER TABLE Friendinvites ADD CONSTRAINT Friendinvites_fk0 FOREIGN KEY (from_u) REFERENCES Users(id);
ALTER TABLE Friendinvites ADD CONSTRAINT Friendinvites_fk1 FOREIGN KEY (to_u) REFERENCES Users(id);

ALTER TABLE Friends ADD CONSTRAINT Friends_fk0 FOREIGN KEY (user_id) REFERENCES Users(id);
ALTER TABLE Friends ADD CONSTRAINT Friends_fk1 FOREIGN KEY (friend_id) REFERENCES Users(id);

ALTER TABLE Chat ADD CONSTRAINT Chat_fk0 FOREIGN KEY (created_by) REFERENCES Users(id);

ALTER TABLE Invites ADD CONSTRAINT Invites_fk0 FOREIGN KEY (chat_id) REFERENCES Chat(id);
ALTER TABLE Invites ADD CONSTRAINT Invites_fk1 FOREIGN KEY (from_u) REFERENCES Users(id);
ALTER TABLE Invites ADD CONSTRAINT Invites_fk2 FOREIGN KEY (to_u) REFERENCES Users(id);

ALTER TABLE Message ADD CONSTRAINT Message_fk0 FOREIGN KEY (autor_id) REFERENCES UserInChat(id);
ALTER TABLE Message ADD CONSTRAINT Message_fk1 FOREIGN KEY (chat_id) REFERENCES Chat(id);

ALTER TABLE UserInChat ADD CONSTRAINT UserInChat_fk0 FOREIGN KEY (chat_id) REFERENCES Chat(id);
ALTER TABLE UserInChat ADD CONSTRAINT UserInChat_fk1 FOREIGN KEY (created_by) REFERENCES Users(id);
ALTER TABLE UserInChat ADD CONSTRAINT UserInChat_fk2 FOREIGN KEY (user_id) REFERENCES Users(id);

ALTER TABLE Conference ADD CONSTRAINT Conference_fk0 FOREIGN KEY (city) REFERENCES City(id);
ALTER TABLE Conference ADD CONSTRAINT Conference_fk1 FOREIGN KEY (chat) REFERENCES Chat(id);

ALTER TABLE Report ADD CONSTRAINT Report_fk0 FOREIGN KEY (autor) REFERENCES Users(id);
ALTER TABLE Report ADD CONSTRAINT Report_fk1 FOREIGN KEY (conference) REFERENCES Conference(id);

ALTER TABLE City ADD CONSTRAINT City_fk0 FOREIGN KEY (country) REFERENCES Country(id);

--ТРИГГЕРЫ

--ТРИГГЕР НА ПОДСЧЕТ ПОЛЬЗОВАТЕЛЕЙ В ЧАТЕ после добавления
CREATE FUNCTION count_in_chats() RETURNS trigger AS $count_in_chats$
BEGIN
		UPDATE Chat ch
		SET numinchat = ((SELECT COUNT(*) FROM  UserInChat uid WHERE uid.chat_id = NEW.chat_id ))
	WHERE ch.id = NEW.chat_id;
        RETURN NULL;
END;
$count_in_chats$ LANGUAGE plpgsql;


CREATE TRIGGER counting_number_users_in_chat
    AFTER INSERT  ON UserInChat
    FOR EACH ROW
    WHEN (NEW.chat_id IS NOT NULL)
    EXECUTE PROCEDURE count_in_chats();

-- после удаления

CREATE FUNCTION count_in_chats_d() RETURNS trigger AS $count_in_chats_d$
BEGIN
		UPDATE Chat ch
		SET numinchat = ((SELECT COUNT(*) FROM  UserInChat uid WHERE uid.chat_id = OLD.chat_id ))
	WHERE ch.id = OLD.chat_id;
        RETURN NULL;
END;
$count_in_chats_d$ LANGUAGE plpgsql;


CREATE TRIGGER counting_number_users_in_chat_d
    AFTER DELETE ON UserInChat
    FOR EACH ROW
    WHEN (OLD.chat_id IS NOT NULL)
    EXECUTE PROCEDURE count_in_chats_d();

---Подсчет активности

CREATE FUNCTION activ_point() RETURNS trigger AS $activ_point$
BEGIN
		UPDATE Users u
		SET activities = ((SELECT COUNT(*) FROM  Report r WHERE r.autor = NEW.autor ))
	WHERE u.id = NEW.autor;
        RETURN NULL;
END;
$activ_point$ LANGUAGE plpgsql;


CREATE TRIGGER counting_activ_point
    AFTER INSERT ON Report
    FOR EACH ROW
    WHEN (NEW.autor IS NOT NULL)
    EXECUTE PROCEDURE activ_point();

---Подсчет популярности

CREATE FUNCTION popularity_point() RETURNS trigger AS $popularity_point$
BEGIN
	UPDATE Users u
	SET popularity = (SELECT COUNT(*)
				FROM  Friends f
				WHERE (f.user_id = u.id) or  (f.friend_id = u.id) 
	)
	WHERE ((u.id = NEW.friend_id)OR(u.id = NEW.user_id));
        RETURN NULL;
END;
$popularity_point$ LANGUAGE plpgsql;

CREATE TRIGGER counting_popularity_point
    AFTER INSERT ON Friends
    FOR EACH ROW
    WHEN (NEW.user_id IS NOT NULL)
    EXECUTE PROCEDURE popularity_point();


-- ХРАНИМЫЕ ФУНКЦИИ
 
CREATE FUNCTION get_user(n text) RETURNS integer AS $$
    SELECT id FROM Users WHERE email = n AND n IS NOT NULL;
$$ LANGUAGE SQL; 
 
CREATE FUNCTION get_city(n text) RETURNS integer AS $$
    SELECT id FROM City WHERE name = n AND n IS NOT NULL;
$$ LANGUAGE SQL; 
 
CREATE FUNCTION get_country(n text) RETURNS integer AS $$
    SELECT id FROM Country WHERE name = n AND n IS NOT NULL;
$$ LANGUAGE SQL; 

-- ЗАПОЛНЕНИЕ ДАННЫМИ

COPY UserType (type) FROM stdin;
Новичок
Опытный
Сторожила
\.

COPY country (name) FROM stdin;
Россия
Украина
Америка
Чехия
Испания
\.


COPY City (name, country) FROM stdin;
Москва	1
Питер	1
Керчь	1
Казань	1
Краснодар	1
Красноярск	1
Лос-Анджелис	3
Калифорния	3
Прага	4
Киево	2
Мадрид	5
\.

COPY Users(email, password, firstname, secondname, city, sex) FROM stdin;
merala326@gmail.com	password	Дима	Пашков	1	м
somemail@mail.ru	password1	Даня	Гришко	1	м
somemail1@mail.ru	password1	Даня	Иванов	1	м
somemail2@mail.ru	password1	Маша	Иванова	2	ж
somemail3@mail.ru	password1	Катя	Иванова	3	ж
somemail4@mail.ru	password1	Света	Иванова	4	ж
somemail5@mail.ru	password1	Георгий	Иванов	5	м
somemail6@mail.ru	password1	Василий	Иванов	6	м
somemail7@mail.ru	password1	Михаил	Иванов	7	м
somemail8@mail.ru	password1	Галина	Иванова	8	ж
somemail9@mail.ru	password1	Даня	Гончаров	9	м
somemail10@mail.ru	password1	Маша	Гончарова	10	ж
somemail11@mail.ru	password1	Катя	Гончарова	11	ж
somemail12@mail.ru	password1	Света	Гончарова	10	ж
somemail13@mail.ru	password1	Георгий	Гончаров	9	м
somemail14@mail.ru	password1	Василий	Гончаров	8	м
somemail15@mail.ru	password1	Михаил	Гончаров	7	м
somemail16@mail.ru	password1	Галина	Гончарова	6	ж
1somemail1@mail.ru	password1	Даня	Химиков	5	м
1somemail2@mail.ru	password1	Маша	Химикова	4	ж
1somemail3@mail.ru	password1	Катя	Химикова	3	ж
1somemail4@mail.ru	password1	Света	Химикова	2	ж
1somemail5@mail.ru	password1	Георгий	Химиков	1	м
1somemail6@mail.ru	password1	Василий	Химиков	2	м
1somemail7@mail.ru	password1	Михаил	Химиков	3	м
1somemail8@mail.ru	password1	Галина	Химикова	4	ж
1somemail9@mail.ru	password1	Даня	Галицын	5	м
1somemail10@mail.ru	password1	Маша	Галицина	6	ж
1somemail11@mail.ru	password1	Катя	Галицина	7	ж
1somemail12@mail.ru	password1	Света	Галицина	8	ж
1somemail13@mail.ru	password1	Георгий	Галицын	9	м
1somemail14@mail.ru	password1	Василий	Галицын	10	м
1somemail15@mail.ru	password1	Михаил	Галицын	11	м
1somemail16@mail.ru	password1	Галина	Галицина	10	ж
\.


COPY Friends(user_id, friend_id) FROM stdin;
1	2
1	3
1	4
1	5
1	6
1	7
1	8
1	9
1	10
1	11
1	12
1	13
1	14
1	15
1	16
1	17
1	18
1	19
1	20
2	3
4	5
6	7
\.

COPY Friendinvites (from_u, to_u) FROM stdin;
1	21
1	23
1	24
1	25
1	26
1	27
1	28
1	29
1	30
31	32
\.

COPY Chat (name, created_at, created_by, level_chat) FROM stdin;
ЧатПары	'2004-10-19 10:23:54'	1	0
ЧатНескольких	'2005-10-19 10:23:54'	1	1
ЧатКонференции	'2004-10-19 10:23:54'	1	2
\.

COPY Invites(chat_id, created_at, from_u, to_u) FROM stdin;
2	'2004-10-19 10:23:55'	1	20
1	'2004-10-19 10:23:56'	1	19
\.

COPY UserInChat (user_id, chat_id, created_by, created_at, user_level) FROM stdin;
1	3	1	'2004-10-19 10:23:56'	2
2	3	1	'2004-10-19 10:23:56'	1
3	3	1	'2004-10-19 10:23:56'	1
4	3	1	'2004-10-19 10:23:56'	1
5	3	1	'2004-10-19 10:23:56'	1
6	3	1	'2004-10-19 10:23:56'	1
7	3	1	'2004-10-19 10:23:56'	1
8	3	1	'2004-10-19 10:23:56'	1
9	3	1	'2004-10-19 10:23:56'	1
\.


COPY Message (autor_id, chat_id, created_at, text) FROM stdin;
1	3	'2004-10-19 10:23:56'	first_message
1	3	'2004-10-19 10:23:56'	second_message
1	3	'2004-10-19 10:23:56'	first_message2
1	3	'2004-10-19 10:23:56'	first_message3
1	3	'2004-10-19 10:23:56'	first_message4
1	3	'2004-10-19 10:23:56'	first_message5
1	3	'2004-10-19 10:23:56'	first_message6
\.



COPY Conference (city, datetime, chat) FROM stdin;
1	'2004-10-19 10:23:56'	2
\.

COPY Report (url, autor, datetime, conference) FROM stdin;
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%231	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%232	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%233	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%234	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%235	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%236	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%237	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%238	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%239	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%2310	1	'2004-10-19 10:23:56'	1
https://gitlab.com/CodingSquire/postgreesql/tree/master/homework%20%2311	1	'2004-10-19 10:23:56'	1
\.


-- ТРИГЕРЫ

-- ХРАНИМЫЕ ФУНКЦИИ

-- ВЫВОД ДАННЫХ

SELECT * FROM Users;

SELECT * FROM UserType;

SELECT * FROM Friendinvites;

SELECT * FROM Friends;

SELECT * FROM Chat;

SELECT * FROM Invites;

SELECT * FROM Message;

SELECT * FROM UserInChat;

SELECT * FROM Conference;

SELECT * FROM Report;

SELECT * FROM City;

SELECT * FROM Country;



