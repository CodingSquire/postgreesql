ДЗ 9. Гл. 10: 
- [x] [3;](https://edu.postgrespro.ru/sql_primer.pdf#page=318)
- [x] [6;](https://edu.postgrespro.ru/sql_primer.pdf#page=320)
- [x] [8.](https://edu.postgrespro.ru/sql_primer.pdf#page=320)

# Глава 10 Задание 3.

Cамостоятельно выполните команду **EXPLAIN** для запроса, содержащего общее табличное выражение (CTE). Посмотрите, на каком уровне находится узел плана, отвечающий за это выражение, как он оформляется. Учтите, что общие табличные выражения всегда материализуются, т. е. вычисляются однократно и результат их вычисления сохраняется в памяти, а затем все последующие обращения в рамках запроса направляются уже к этому материализованному результату

---

```
EXPLAIN ANALYZE WITH RECURSIVE ranges ( min_sum, max_sum, iteration )
AS (
	VALUES( 0,      100000, 0 ),
		  ( 100000, 200000, 0 ),
		  ( 200000, 300000, 0 )
UNION
SELECT min_sum + 100000, max_sum + 100000, iteration + 1
	FROM ranges
	WHERE max_sum < ( SELECT max( total_amount ) FROM bookings )
)
SELECT DISTINCT iteration, count(iteration) FROM ranges 
GROUP BY iteration 
ORDER BY iteration;
```

![9_10_3.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%239/img/9_10_3.png)

[ссылка на схему](https://explain.depesz.com/s/cJjr)

---

# Глава 10 Задание 6.

Выполните команду **EXPLAIN** для запроса, в котором использована какая-нибудь из оконных функций. Найдите в плане выполнения запроса узел с именем WindowAgg. Попробуйте объяснить, почему он занимает именно этот уровень в плане.

---

```
EXPLAIN ANALYZE SELECT b.book_ref,
	   b.book_date,
	   extract('month'from b.book_date ) AS month,
	   extract('day'from b.book_date ) AS day,
	   count( * ) OVER (
		PARTITION BY date_trunc('month', b.book_date )
		ORDER BY b.book_date) 
	   AS count
  FROM ticket_flights tf
  JOIN tickets  t ON tf.ticket_no = t.ticket_no
  JOIN bookings b ON t.book_ref = b.book_ref
  WHERE tf.flight_id = 1
  ORDER BY b.book_date;
```

![9_10_6.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%239/img/9_10_6.png)

[ссылка на схему](https://explain.depesz.com/s/2Fbxa)

---

# Глава 10 Задание 8.

Замена коррелированного подзапроса соединением таблиц является одним из способов повышения производительности.

Предположим, что мы задались вопросом: сколько маршрутов обслуживают самолеты каждого типа? При этом нужно учитывать, что может иметь место такая ситуация, когда самолеты какого-либо типа не обслуживают ни одного маршрута. Поэтому необходимо использовать не только представление «Маршруты» (*routes*), но и таблицу «Самолеты» (*aircrafts*).

Это первый вариант запроса, в нем используется коррелированный подзапрос.

```
EXPLAIN ANALYZE
    SELECT a.aircraft_code AS a_code,
           a.model,
           ( SELECT count( r.aircraft_code )
                FROM routes r
                WHERE r.aircraft_code = a.aircraft_code
           ) AS num_routes
    FROM aircrafts a
    GROUP BY 1, 2
    ORDER BY 3 DESC;
```

А в этом варианте коррелированный подзапрос раскрыт и заменен внешним соединением:

```
EXPLAIN ANALYZE
    SELECT a.aircraft_code AS a_code,
           a.model,
           count( r.aircraft_code ) AS num_routes
    FROM aircrafts a
    LEFT OUTER JOIN routes r
        ON r.aircraft_code = a.aircraft_code
    GROUP BY 1, 2
    ORDER BY 3 DESC;
```

Причина использования внешнего соединения в том, что может найтись модель самолета, не обслуживающая ни одного маршрута, и если не использовать внешнее соединение, она вообще не попадет в результирующую выборку.

Исследуйте планы выполнения обоих запросов. Попытайтесь найти объяснение различиям в эффективности их выполнения. Чтобы получить усредненную картину, выполните каждый запрос несколько раз. Поскольку таблицы, участвующие в запросах, небольшие, то различие по абсолютным затратам времени выполнения будет незначительным. Но если бы число строк в таблицах было большим, то экономия ресурсов сервера могла оказаться заметной.

Предложите аналогичную пару запросов к базе данных «Авиаперевозки». Проведите необходимые эксперименты с вашими запросами.

---

```
EXPLAIN ANALYZE
SELECT  a.aircraft_code AS a_code,
		a.model,
		( SELECT count( r.aircraft_code )
			FROM routes r
			WHERE r.aircraft_code = a.aircraft_code) AS num_routes
	FROM aircrafts a
	GROUP BY 1, 2
	ORDER BY 3 DESC;
```

![9_10_8_1.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%239/img/9_10_8_1.png)

[ссылка на схему](https://explain.depesz.com/s/SqxL)

---

```
EXPLAIN ANALYZE
SELECT  a.aircraft_code AS a_code,
		a.model,
		count( r.aircraft_code ) AS num_routes
	FROM aircrafts a
	LEFT OUTER JOIN routes r
		ON r.aircraft_code = a.aircraft_code
	GROUP BY 1, 2
	ORDER BY 3 DESC;
```

![9_10_8_2.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%239/img/9_10_8_2.png)

[ссылка на схему](https://explain.depesz.com/s/jWej)

---

```
EXPLAIN ANALYZE SELECT flight_no, departure_city, arrival_city
FROM routes
WHERE departure_city IN 
		(SELECT city
			FROM airports
			WHERE timezone ~'Krasnoyarsk')
  AND arrival_city IN 
		(SELECT city
			FROM airports
			WHERE timezone ~'Krasnoyarsk');
```

![9_10_8_3.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%239/img/9_10_8_3.png)

[ссылка на схему](https://explain.depesz.com/s/ACKV)

---

```
EXPLAIN ANALYZE SELECT flight_no, departure_city, arrival_city
FROM routes r
 JOIN airports d_a ON (
						d_a.city = r.departure_city
						AND d_a.timezone ~'Krasnoyarsk')
 JOIN airports a_a ON (
						a_a.city = r.arrival_city
						AND a_a.timezone ~'Krasnoyarsk'); 
```

![9_10_8_4.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%239/img/9_10_8_4.png)

[ссылка на схему](https://explain.depesz.com/s/6ZZr)

---








