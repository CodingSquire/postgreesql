ДЗ 6. Гл. 7: 
- [ ] [1;](https://edu.postgrespro.ru/sql_primer.pdf#page=227)
- [ ] [2;](https://edu.postgrespro.ru/sql_primer.pdf#page=227)
- [ ] [4.](https://edu.postgrespro.ru/sql_primer.pdf#page=22)


# Глава 7 Задание 1.

Добавьте в определение таблицы *aircrafts_log* значение по умолчанию *current_timestamp* и соответствующим образом измените команды **INSERT**, приведенные в тексте главы.

---

```
CREATE TEMP TABLE aircrafts_tmp AS
SELECT * FROM aircrafts WITH NO DATA;

ALTER TABLE aircrafts_tmp
ADD PRIMARY KEY ( aircraft_code );

ALTER TABLE aircrafts_tmp
ADD UNIQUE ( model );

CREATE TEMP TABLE aircrafts_log AS
SELECT * FROM aircrafts WITH NO DATA;

ALTER TABLE aircrafts_log 
ADD COLUMN when_add timestamp DEFAULT current_timestamp;

ALTER TABLE aircrafts_log
ADD COLUMN operation text;

WITH add_row AS( 
INSERT INTO aircrafts_tmp
SELECT * FROM aircrafts
RETURNING *)

INSERT INTO aircrafts_log (aircraft_code, model, range, operation)
SELECT add_row.aircraft_code, add_row.model, add_row.range, 'INSERT'
FROM add_row;
```
![6_7_1.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%236/img/6_7_1.png)
---

# Глава 7 Задание 2.

В предложении **RETURNING** можно указывать не только символ «∗», означающий выбор всех столбцов таблицы, но и более сложные выражения, сформированные на основе этих столбцов. В тексте главы мы копировали содержимое таблицы «Самолеты» в таблицу *aircrafts_tmp*, используя в предложении **RETURNING** именно «∗». Однако возможен и другой вариант запроса:

```
WITH add_row AS
( INSERT INTO aircrafts_tmp
    SELECT * FROM aircrafts
    RETURNING aircraft_code, model, range,
              current_timestamp, 'INSERT'
)
INSERT INTO aircrafts_log
    SELECT ? FROM add_row;
```

---

### Что нужно написать в этом запросе вместо вопросительного знака?

---

```
CREATE TEMP TABLE aircrafts_tmp AS
SELECT * FROM aircrafts WITH NO DATA;

ALTER TABLE aircrafts_tmp
ADD PRIMARY KEY ( aircraft_code );

ALTER TABLE aircrafts_tmp
ADD UNIQUE ( model );

CREATE TEMP TABLE aircrafts_log AS
SELECT * FROM aircrafts WITH NO DATA;

ALTER TABLE aircrafts_log 
ADD COLUMN when_add timestamp;

ALTER TABLE aircrafts_log
ADD COLUMN operation text;

WITH add_row AS( 
INSERT INTO aircrafts_tmp 
SELECT * 
FROM aircrafts
RETURNING aircraft_code, model, range, current_timestamp,'INSERT')

INSERT INTO aircrafts_log
SELECT * 
FROM add_row;
```

![6_7_2.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%236/img/6_7_2.png)


---

# Глава 7 Задание 4.
 
 В тексте главы в предложениях **ON CONFLICT** команды **INSERT** мы использовали только выражения, состоящие из имени одного столбца. Однако в таблице «Места» (*seats*) первичный ключ является составным и включает два столбца.
 
 Напишите команду **INSERT** для вставки новой строки в эту таблицу и предусмотрите возможный конфликт добавляемой строки со строкой, уже имеющейся в таблице. Сделайте два варианта предложения **ON CONFLICT**: первый — с использованием перечисления имен столбцов для проверки наличия дублирования, второй — с использованием предложения **ON CONSTRAINT**.
 
 Для того чтобы не изменить содержимое таблицы «Места», создайте ее копию и выполняйте все эти эксперименты с таблицей-копией.

---

```
CREATE TEMP TABLE seats_tmp AS
SELECT * FROM seats;

ALTER TABLE seats_tmp
ADD CONSTRAINT uniq UNIQUE (aircraft_code, seat_no, fare_conditions);

INSERT INTO seats_tmp
	VALUES (773, '49D' ,'Economy')
	ON CONFLICT ( aircraft_code, seat_no, fare_conditions ) DO NOTHING
	RETURNING *;

INSERT INTO seats_tmp
	VALUES (773, '49D' ,'Economy')
	ON CONFLICT ON CONSTRAINT uniq DO NOTHING
	RETURNING *;
```
---

![6_7_4.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%236/img/6_7_4.png)


