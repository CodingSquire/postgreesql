
--выведет информацию о месте проживани пользователя и его уровне аккаунта
SELECT u.email, u.firstname, u.secondname, ut.type, c.name as city, cc.name AS country 
FROM Users u 
LEFT JOIN UserType ut ON u.type=ut.id 
LEFT JOIN City c ON u.city=c.id 
LEFT JOIN country cc ON c.country=cc.id; 


--выведет количество отправленных запросов на дружбу чатов
SELECT u.email, u.firstname, u.secondname, count(*)
FROM Users u 
LEFT JOIN Invites i ON u.id=i.from_u Group By u.email, u.firstname, u.secondname; 


--выведет всех сообщений вместе с автором и датой публикации
SELECT u.email, u.firstname, u.secondname, c.name, m.created_at, m.text
FROM Users u
RIGHT JOIN UserInChat uic ON u.id=uic.user_id
RIGHT JOIN Chat c ON c.id=uic.chat_id
RIGHT JOIN Message m ON u.id=m.autor_id;

	

--Принятие запроса на дружбу
with moved_rows AS (
	DELETE FROM Friendinvites
	WHERE id=1
	RETURNING *
)
INSERT INTO Friends (user_id,friend_id)
SELECT from_u,to_u FROM moved_rows;

-- Оконная функция подсчет ученых рядом

SELECT u.email, u.firstname, u.secondname, c.name as city,
count(*) OVER(
PARTITION BY u.city ORDER BY u.city) as "scientists nearby"
FROM Users u 
LEFT JOIN City c ON u.city=c.id ;


