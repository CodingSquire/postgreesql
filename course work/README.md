# ПЕРВАЯ ЧАСТЬ

# Краткое описание.

Мной будет разрабатываться База Данных Социальной сети для научного сообщества, которая бы позволяла хранить: 

- личные профели исследователей (ФИО, связь со внешними сервисами, почта, и т.д.) 
- информацию о чаты и сообщениями
- научные доклады
- информацию о конференциях
- различные уровни активности и популярности исследователей
- связи "дружбы" между научными работниками

---

Есть сущности:
- **User** - пользователь;
- **Chat** - чат с сообщениями; 
- **Message** - сообщения;
- **Conference** - конференция;
- **Report** - научный доклад.

---

# User - Пользователь

Пользователь обладает личным профилем.
Пользователи могут:

- **добавлять** друг друга в *друзья*
- **создавать** *чаты*
- **добавлять** друзей *в чаты*
- **обмениваться** *сообщениями* в чатах
- **регистрировать** свои *Доклады* на конференциях

|Users      |           |                                       |
| ----------|:---------:|---------------------------------------|
|**name**   |**type**   |**description**                        |
|id         |integer    |Идентификатор                          |
|email      |text       |Адрес электронной почты                |
|password   |text       |Пароль                                 |
|firstname  |text       |Имя                                    |
|secondname |text       |Фамилия                                |
|city       |integer    |Город проживания                       |
|about      |text       |Описание профиля                       |
|birthday   |datetime   |Дата рождения                          |
|datacreate |datetime   |Дата создания профиля                  |
|vk_url     |text       |URL-на страницу ВК                     |
|twitter_url|text       |URL-на страницу twitter                |
|phone      |text       |Номер телефона                         |
|sex        |text       |Пол                                    |
|type       |integer    |Тип пользователя                                    |
|popularity |numeric    |Показатель популярности пользователя   |
|activities |numeric    |Показатель активности пользователя     |

---

## Пользователи бывают разных типов

Типы пользователей:

- **Новичк** - новый пользователь;
- **Опытный** - пользователь опубликовавший несколько докладов;
- **Сторожила** - популярные пользователи, которые могут создавать конференции.

 
|UserType   |           |                   |
| ----------|:---------:|-------------------|
|**name**   |**type**   |**description**    |
|id         |integer    |Идентификатор      |
|type       |text       |Тип пользователя   |

---

## Отношение ДРУЖБЫ

**Пользователи** могут отправлять друг другу *запросы* на **ДРУЖБУ**

|Friendinvites  |           |                   |
| --------------|:---------:|-------------------|
|**name**       |**type**   |**description**    |
|id             |integer    |Идентификатор      |
|from           |integer    |Отправитель запроса|
|to             |integer    |Получатель запроса |

В случае подтверждения *запроса* пользователи переходят в отношения **ДРУЖБЫ**, что позволит добавлять друг друга в чаты.

|Friends    |           |                                   |
| ----------|:---------:|-----------------------------------|
|**name**   |**type**   |**description**                    |
|id         |integer    |Идентификатор                      |
|user_id    |integer    |Пользователь инициировавший дружбу |
|friend_id  |integer    |Пользователь принявший дружбу      |

# Chat - Чат

**Чаты** позволяют **пользователям** обмениваться **сообщениями**.

**Чаты** бывают нескольких **уровней**:
- **0** - чат двух для двух пользователей
- **1** - чат двух для не более пяти пользователей
- **2** - чат конференции

|Chat           |           |                           |
| --------------|:---------:|---------------------------|
|**name**       |**type**   |**description**            |
|id             |integer    |Идентификатор              |
|name           |text       |Название чата              |
|created_at     |DATA       |Дата создания чата         |
|created_by     |integer    |Создатель чата             |
|level_chat     |integer    |Уровень чата               |
|numinchat      |integer    |количество человек в чате  |
---

**Пользователи** находящиеся в отношении **ДРУЖБЫ** отправлять друг другу **запросы** на добавление в чат.

|Invites        |           |                   |
| --------------|:---------:|-------------------|
|**name**       |**type**   |**description**    |
|id             |integer    |Идентификатор      |
|chat_id        |integer    |Идентификатор чата |
|created_at     |DATA       |Дата создания чата |
|from           |integer    |Отправитель запроса|
|to             |integer    |Получатель запроса |


---

# Message - сообщение

|Message        |           |                           |
| --------------|:---------:|---------------------------|
|**name**       |**type**   |**description**            |
|id             |integer    |Идентификатор              |
|autor_id       |integer    |Идентификатор автора       |
|chat_id        |integer    |Идентификатор Чата         |
|created_at     |DATA       |Дата отправления сообщения | 
|text           |text       |Текст сообщения            |



### UserInChat - Пользователь в Чате 

Пользователь в Чате может обладать следующими уровнями:

- **0** - доступно только чтение сообщений;
- **1** - права нулевого уровня и добавление новых сообщений; 
- **2** - права первого уровня и добавление новых пользователей (если это чат конференции - добавление докладов). 


|UserInChat     |           |                       |
| --------------|:---------:|-----------------------|
|**name**       |**type**   |**description**        |
|id             |integer    |Идентификатор          |
|user_id        |integer    |Идентификатор Юзера    |
|chat_id        |integer    |Идентификатор чата     |
|created_at     |DATA       |Дата добавления        |
|created_by     |integer    |Пригласивший           |
|user_level     |integer    |Уровень пользователя   |


---

# Conference - Конференция

Конференции могут создавать только пользователи *типа* **Сторожила**. При создании конференции также создается чат конференции.



|Conference     |           |                               |
| --------------|:---------:|-------------------------------|
|**name**       |**type**   |**description**                |
|id             |integer    |Идентификатор                  |
|city           |integer    |Город проведения конференции   |
|datetime       |DATA       |Дата проведения конференции    |
|chat           |integer    |Официальный чат конференции    |

----

### Report - Доклад

|Report         |           |                           |
| --------------|:---------:|---------------------------|
|**name**       |**type**   |**description**            |
|id             |integer    |Идентификатор              |
|url            |text       |URL ссылка на доклад       |
|autor          |integer    |Автор отчета               |
|datetime       |DATA       |Дата регистрации доклада   |
|conference     |integer    |Конференция                |

---

### City - Город

|City           |           |               |
| --------------|:---------:|---------------|
|**name**       |**type**   |**description**|
|id             |integer    |Идентификатор  |
|name           |text       |Название города|
|country        |integer    |Cтрана         |

---

### Country - Страна

|Country        |           |               |
| --------------|:---------:|---------------|
|**name**       |**type**   |**description**|
|id             |integer    |Идентификатор  |
|name           |text       |Название страны|


## Концептуальная модель данных с использованием ER-диаграмм

![conceptual_model.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/conceptual_model.png)

---

## Логическая модель

![logical_model.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/logical_model.png)

---

## Физическая модель

![fisical_model.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/fisical_model.png)

---

##  Физическое проектирование

[Весь](https://gitlab.com/CodingSquire/postgreesql/tree/master/course%20work/sql) SQL код разделен на 3 группы:
- [Инициализация БД](https://gitlab.com/CodingSquire/postgreesql/blob/master/course%20work/sql/init.sql)
- [Запросы БД](https://gitlab.com/CodingSquire/postgreesql/blob/master/course%20work/sql/requset.sql)


---

# ВТОРАЯ ЧАСТЬ

## Инициализация

![](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/initializ/1.png)

---

![](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/initializ/2.png)

---

![](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/initializ/3.png)

---

![](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/initializ/4.png)

---

![](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/initializ/5.png)

---

## Тригеры
### Проверка добавления в чат
**Условие:**

- *добавляющий должен быть в друзья*

**Срабатывает:**

- при попытке добавления в таблицу *Invites*

**Описание:**

- запрос на совпадение пары from+to или пары to from в таблице friends
- при успехе добавить поле
- иначе выдать ошибку

### Проверка создания конференции

**Условие:**

- *добавляющий должен быть уровня сторожила*
 
**Срабатывает:**

- при попытке добавления в таблицу *Conference*

**Описание:**

- запрос на совпадение type = 2 (пользователь сторожила)
- при неудаче выдать ошибку
- при успехе:
    - добаввить в таблицу Chat вое поле и задать уровень чата = 2
    - Добавить поле в таблицу *Conference* и задать чат 

### Подсчет пользователей в чате

- После входа в чат
 
```
CREATE FUNCTION count_in_chats() RETURNS trigger AS $count_in_chats$
BEGIN
		UPDATE Chat ch
		SET numinchat = ((SELECT COUNT(*) FROM  UserInChat uid WHERE uid.chat_id = NEW.chat_id ))
	WHERE ch.id = NEW.chat_id;
        RETURN NULL;
END;
$count_in_chats$ LANGUAGE plpgsql;


CREATE TRIGGER counting_number_users_in_chat
    AFTER INSERT  ON UserInChat
    FOR EACH ROW
    WHEN (NEW.chat_id IS NOT NULL)
    EXECUTE PROCEDURE count_in_chats();
```

- После выхода из чата

```
CREATE FUNCTION count_in_chats_d() RETURNS trigger AS $count_in_chats_d$
BEGIN
		UPDATE Chat ch
		SET numinchat = ((SELECT COUNT(*) FROM  UserInChat uid WHERE uid.chat_id = OLD.chat_id ))
	WHERE ch.id = OLD.chat_id;
        RETURN NULL;
END;
$count_in_chats_d$ LANGUAGE plpgsql;


CREATE TRIGGER counting_number_users_in_chat_d
    AFTER DELETE ON UserInChat
    FOR EACH ROW
    WHEN (OLD.chat_id IS NOT NULL)
    EXECUTE PROCEDURE count_in_chats_d();
```

---

### Подсчет активности

```
CREATE FUNCTION activ_point() RETURNS trigger AS $activ_point$
BEGIN
		UPDATE Users u
		SET activities = ((SELECT COUNT(*) FROM  Report r WHERE r.autor = NEW.autor ))
	WHERE u.id = NEW.autor;
        RETURN NULL;
END;
$activ_point$ LANGUAGE plpgsql;


CREATE TRIGGER counting_activ_point
    AFTER INSERT ON Report
    FOR EACH ROW
    WHEN (NEW.autor IS NOT NULL)
    EXECUTE PROCEDURE activ_point();
```

---

### Подсчет популярности

```
CREATE FUNCTION popularity_point() RETURNS trigger AS $popularity_point$
BEGIN
	UPDATE Users u
	SET popularity = (SELECT COUNT(*)
				FROM  Friends f
				WHERE (f.user_id = u.id) or  (f.friend_id = u.id) 
	)
	WHERE ((u.id = NEW.friend_id)OR(u.id = NEW.user_id));
        RETURN NULL;
END;
$popularity_point$ LANGUAGE plpgsql;

CREATE TRIGGER counting_popularity_point
    AFTER INSERT ON Friends
    FOR EACH ROW
    WHEN (NEW.user_id IS NOT NULL)
    EXECUTE PROCEDURE popularity_point();
```

---

## Хранимые фунции 

```
CREATE FUNCTION get_user(n text) RETURNS integer AS $$
    SELECT id FROM Users WHERE email = n AND n IS NOT NULL;
$$ LANGUAGE SQL; 
 
CREATE FUNCTION get_city(n text) RETURNS integer AS $$
    SELECT id FROM City WHERE name = n AND n IS NOT NULL;
$$ LANGUAGE SQL; 
 
CREATE FUNCTION get_country(n text) RETURNS integer AS $$
    SELECT id FROM Country WHERE name = n AND n IS NOT NULL;
$$ LANGUAGE SQL; 
```

---


## Выполнение запросов


![](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/request/1.png)

---

![](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/request/2.png)

---

![](https://gitlab.com/CodingSquire/postgreesql/raw/master/course%20work/img/request/3.png)

---