# ДЗ 1. Элементы теории баз данных .

# Задание:

* Подумать над выбором предметной области для выполнения финальной (зачетной) работы.
    * Выбирайте предметную область, которая вам интересна и в которой вы разбираетесь или хотите разобраться.
    * Сделать краткое описание выбранной предметной области (1-2 страницы). Это описание затем войдет в финальный отчет.
* Попытаться сформулировать требования к будущей базе данных (можно опираться
на вводную лекцию). Все презентации лекций находятся здесь:
https://postgrespro.ru/education/university/sqlprimer 

---

# Краткое описание.

Мной будет разрабатываться База Данных Социальной сети для научного сообщества, которая бы позволяла хранить: 

- личные профели исследователей (ФИО, связь со внешними сервисами, почта, и т.д.) 
- информацию о чаты и сообщениями
- научные доклады
- информацию о конференциях
- различные уровни активности и популярности исследователей
- связи "дружбы" между научными работниками

---

Есть сущности:
- **User** - пользователь;
- **Chat** - чат с сообщениями; 
- **Message** - сообщения;
- **Conference** - конференция;
- **Report** - научный доклад.

---

# User - Пользователь

Пользователь обладает личным профилем.
Пользователи могут:

- **добавлять** друг друга в *друзья*
- **создавать** *чаты*
- **добавлять** друзей *в чаты*
- **обмениваться** *сообщениями* в чатах
- **регистрировать** свои *Доклады* на конференциях

|Users      |           |                                       |
| ----------|:---------:|---------------------------------------|
|**name**   |**type**   |**description**                        |
|id         |integer    |Идентификатор                          |
|email      |text       |Адрес электронной почты                |
|password   |text       |Пароль                                 |
|firstname  |text       |Имя                                    |
|secondname |text       |Фамилия                                |
|city       |integer    |Город проживания                       |
|about      |text       |Описание профиля                       |
|birthday   |datetime   |Дата рождения                          |
|datacreate |datetime   |Дата создания профиля                  |
|vk_url     |text       |URL-на страницу ВК                     |
|twitter_url|text       |URL-на страницу twitter                |
|phone      |text       |Номер телефона                         |
|sex        |text       |Пол                                    |
|type       |integer    |Тип пользователя                                    |
|popularity |numeric    |Показатель популярности пользователя   |
|activities |numeric    |Показатель активности пользователя     |

---

## Пользователи бывают разных типов

Типы пользователей:

- **Новичок** - новый пользователь;
- **Опытный** - пользователь опубликовавший несколько докладов;
- **Старожила** - популярные пользователи, которые могут создавать конференции.

 
|UserType   |           |                   |
| ----------|:---------:|-------------------|
|**name**   |**type**   |**description**    |
|id         |integer    |Идентификатор      |
|type       |text       |Тип пользователя   |

---

## Отношение ДРУЖБЫ

**Пользователи** могут отправлять друг другу *запросы* на **ДРУЖБУ**

|Friendinvites  |           |                   |
| --------------|:---------:|-------------------|
|**name**       |**type**   |**description**    |
|id             |integer    |Идентификатор      |
|from           |integer    |Отправитель запроса|
|to             |integer    |Получатель запроса |

В случае подтверждения *запроса* пользователи переходят в отношения **ДРУЖБЫ**, что позволит добавлять друг друга в чаты.

|Friends    |           |                                   |
| ----------|:---------:|-----------------------------------|
|**name**   |**type**   |**description**                    |
|id         |integer    |Идентификатор                      |
|user_id    |integer    |Пользователь инициировавший дружбу |
|friend_id  |integer    |Пользователь принявший дружбу      |

# Chat - Чат

**Чаты** позволяют **пользователям** обмениваться **сообщениями**.

**Чаты** бывают нескольких **уровней**:
- **0** - чат для двух пользователей
- **1** - чат для не более пяти пользователей
- **2** - чат конференции

|Chat           |           |                           |
| --------------|:---------:|---------------------------|
|**name**       |**type**   |**description**            |
|id             |integer    |Идентификатор              |
|name           |text       |Название чата              |
|created_at     |DATA       |Дата создания чата         |
|created_by     |integer    |Создатель чата             |
|level_chat     |integer    |Уровень чата               |
|numinchat      |integer    |количество человек в чате  |
---

**Пользователи** находящиеся в отношении **ДРУЖБЫ** отправлять друг другу **запросы** на добавление в чат.

|Invites        |           |                   |
| --------------|:---------:|-------------------|
|**name**       |**type**   |**description**    |
|id             |integer    |Идентификатор      |
|chat_id        |integer    |Идентификатор чата |
|created_at     |DATA       |Дата создания чата |
|from           |integer    |Отправитель запроса|
|to             |integer    |Получатель запроса |


---

# Message - сообщение

|Message        |           |                           |
| --------------|:---------:|---------------------------|
|**name**       |**type**   |**description**            |
|id             |integer    |Идентификатор              |
|autor_id       |integer    |Идентификатор автора       |
|chat_id        |integer    |Идентификатор Чата         |
|created_at     |DATA       |Дата отправления сообщения | 
|text           |text       |Текст сообщения            |



### UserInChat - Пользователь в Чате 

Пользователь в Чате может обладать следующими уровнями:

- **0** - доступно только чтение сообщений;
- **1** - права нулевого уровня и добавление новых сообщений; 
- **2** - права первого уровня и добавление новых пользователей (если это чат конференции - добавление докладов). 


|UserInChat     |           |                       |
| --------------|:---------:|-----------------------|
|**name**       |**type**   |**description**        |
|id             |integer    |Идентификатор          |
|chat_id        |integer    |Идентификатор чата     |
|created_at     |DATA       |Дата добавления        |
|created_by     |integer    |Пригласивший           |
|user_level     |integer    |Уровень пользователя   |


---

# Conference - Конференция

Конференции могут создавать только пользователи *типа* **Сторожила**. При создании конференции также создается чат конференции.



|Conference     |           |                               |
| --------------|:---------:|-------------------------------|
|**name**       |**type**   |**description**                |
|id             |integer    |Идентификатор                  |
|city           |integer    |Город проведения конференции   |
|datetime       |DATA       |Дата проведения конференции    |
|chat           |integer    |Официальный чат конференции    |

----

### Report - Доклад

|Report         |           |                           |
| --------------|:---------:|---------------------------|
|**name**       |**type**   |**description**            |
|id             |integer    |Идентификатор              |
|url            |text       |URL ссылка на доклад       |
|autor          |integer    |Автор отчета               |
|datetime       |DATA       |Дата регистрации доклада   |
|conference     |integer    |Конференция                |

---

### City - Город

|City           |           |               |
| --------------|:---------:|---------------|
|**name**       |**type**   |**description**|
|id             |integer    |Идентификатор  |
|name           |text       |Название города|
|country        |integer    |Cтрана         |

---

### Country - Страна

|Country        |           |               |
| --------------|:---------:|---------------|
|**name**       |**type**   |**description**|
|id             |integer    |Идентификатор  |
|name           |text       |Название страны|


# СХЕМА:
![](https://gitlab.com/CodingSquire/postgreesql/raw/develop/homework%20%231/img/logical_model.png)
https://dbdesigner.page.link/zuq4



код схемы:
```
CREATE TABLE "Users" (
	"id" serial,
	"email" TEXT UNIQUE,
	"password" TEXT NOT NULL,
	"firstname" TEXT NOT NULL,
	"secondname" TEXT NOT NULL,
	"city" integer NOT NULL,
	"about" TEXT NOT NULL,
	"birthday" DATETIME NOT NULL,
	"datacreate" DATETIME NOT NULL,
	"vk_url" TEXT NOT NULL,
	"twitter_url" TEXT NOT NULL,
	"phone" TEXT NOT NULL,
	"sex" TEXT NOT NULL,
	"type" integer NOT NULL DEFAULT '0',
	"popularity" FLOAT NOT NULL,
	"activities" FLOAT NOT NULL,
	CONSTRAINT "Users_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "UserType" (
	"id" serial NOT NULL,
	"type" TEXT NOT NULL,
	CONSTRAINT "UserType_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Friendinvites" (
	"id" serial NOT NULL,
	"from" integer NOT NULL,
	"to" integer NOT NULL,
	CONSTRAINT "Friendinvites_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Friends" (
	"id" serial NOT NULL,
	"user_id" integer NOT NULL,
	"friend_id" integer NOT NULL,
	CONSTRAINT "Friends_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Chat" (
	"id" serial NOT NULL,
	"name" TEXT NOT NULL,
	"created_at" DATETIME NOT NULL,
	"created_by" integer NOT NULL,
	"level_chat" integer NOT NULL,
	"numinchat" integer NOT NULL,
	CONSTRAINT "Chat_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Invites" (
	"id" serial NOT NULL,
	"chat_id" integer NOT NULL,
	"created_at" DATETIME NOT NULL,
	"from" integer NOT NULL,
	"to" integer NOT NULL,
	CONSTRAINT "Invites_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Message" (
	"id" serial NOT NULL,
	"autor_id" integer NOT NULL,
	"chat_id" integer NOT NULL,
	"created_at" DATETIME NOT NULL,
	"text" TEXT NOT NULL,
	CONSTRAINT "Message_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "UserInChat" (
	"id" serial NOT NULL,
	"chat_id" integer NOT NULL,
	"created_by" integer NOT NULL,
	"created_at" DATETIME NOT NULL,
	"user_level" integer NOT NULL,
	CONSTRAINT "UserInChat_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Conference" (
	"id" serial NOT NULL,
	"city" integer NOT NULL,
	"datetime" DATETIME NOT NULL,
	"chat" integer NOT NULL,
	CONSTRAINT "Conference_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Report" (
	"id" serial NOT NULL,
	"url" TEXT NOT NULL,
	"autor" integer NOT NULL,
	"datetime" DATETIME NOT NULL,
	"conference" integer NOT NULL,
	CONSTRAINT "Report_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "City" (
	"id" serial NOT NULL,
	"name" TEXT NOT NULL UNIQUE,
	"country" integer NOT NULL,
	CONSTRAINT "City_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Country" (
	"id" serial NOT NULL,
	"name" TEXT NOT NULL UNIQUE,
	CONSTRAINT "Country_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);




ALTER TABLE "Users" ADD CONSTRAINT "Users_fk0" FOREIGN KEY ("city") REFERENCES "City"("id");
ALTER TABLE "Users" ADD CONSTRAINT "Users_fk1" FOREIGN KEY ("type") REFERENCES "UserType"("id");
ALTER TABLE "Users" ADD CONSTRAINT "Users_fk1" FOREIGN KEY ("type") REFERENCES "UserType"("id");

ALTER TABLE "Friendinvites" ADD CONSTRAINT "Friendinvites_fk0" FOREIGN KEY ("from") REFERENCES "Users"("id");
ALTER TABLE "Friendinvites" ADD CONSTRAINT "Friendinvites_fk1" FOREIGN KEY ("to") REFERENCES "Users"("id");

ALTER TABLE "Friends" ADD CONSTRAINT "Friends_fk0" FOREIGN KEY ("user_id") REFERENCES "Users"("id");
ALTER TABLE "Friends" ADD CONSTRAINT "Friends_fk1" FOREIGN KEY ("friend_id") REFERENCES "Users"("id");

ALTER TABLE "Chat" ADD CONSTRAINT "Chat_fk0" FOREIGN KEY ("created_by") REFERENCES "Users"("id");

ALTER TABLE "Invites" ADD CONSTRAINT "Invites_fk0" FOREIGN KEY ("chat_id") REFERENCES "Chat"("id");
ALTER TABLE "Invites" ADD CONSTRAINT "Invites_fk1" FOREIGN KEY ("from") REFERENCES "Users"("id");
ALTER TABLE "Invites" ADD CONSTRAINT "Invites_fk2" FOREIGN KEY ("to") REFERENCES "Users"("id");

ALTER TABLE "Message" ADD CONSTRAINT "Message_fk0" FOREIGN KEY ("autor_id") REFERENCES "UserInChat"("id");
ALTER TABLE "Message" ADD CONSTRAINT "Message_fk1" FOREIGN KEY ("chat_id") REFERENCES "Chat"("id");

ALTER TABLE "UserInChat" ADD CONSTRAINT "UserInChat_fk0" FOREIGN KEY ("chat_id") REFERENCES "Chat"("id");
ALTER TABLE "UserInChat" ADD CONSTRAINT "UserInChat_fk1" FOREIGN KEY ("created_by") REFERENCES "Users"("id");

ALTER TABLE "Conference" ADD CONSTRAINT "Conference_fk0" FOREIGN KEY ("city") REFERENCES "City"("id");
ALTER TABLE "Conference" ADD CONSTRAINT "Conference_fk1" FOREIGN KEY ("chat") REFERENCES "Chat"("id");

ALTER TABLE "Report" ADD CONSTRAINT "Report_fk0" FOREIGN KEY ("autor") REFERENCES "Users"("id");
ALTER TABLE "Report" ADD CONSTRAINT "Report_fk1" FOREIGN KEY ("conference") REFERENCES "Conference"("id");

ALTER TABLE "City" ADD CONSTRAINT "City_fk0" FOREIGN KEY ("country") REFERENCES "Country"("id");
ALTER TABLE "City" ADD CONSTRAINT "City_fk0" FOREIGN KEY ("country") REFERENCES "Country"("id");

```

